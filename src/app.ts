import express from 'express';
import cors from 'cors';
import mongoose from 'mongoose';

import routes from './routes/routes';

const app = express();

try {
    mongoose.connect('mongodb://necromunda:underhive@necromunda_db:27017/necromunda2?authSource=admin', { useNewUrlParser: true })
} catch (error) {
    console.log("Error connecting");
}

app.use(cors());
app.use(express.json()) // for parsing application/json

app.use("/", routes);

app.listen(7878);