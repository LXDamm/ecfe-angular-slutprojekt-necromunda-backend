import { Document, Model, model, Schema } from 'mongoose';
import { gangSchema, GangShape } from './gang';

interface PlayerShape extends Document {
    uid: string;
    name: string;
    gangIds?: [string];
  }

const playerSchema = new Schema(
    {
        uid: {
            type: String,
            required: true,
            unique: true
        },
        name: String,
        gangIds: {
            type: [String],
            required: false,
            default: new Array()
        }
    }
);

const PlayerModel: Model<PlayerShape> = model("Player", playerSchema);

export { PlayerShape, playerSchema, PlayerModel };