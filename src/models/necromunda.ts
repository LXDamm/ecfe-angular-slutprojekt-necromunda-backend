import { Document, Schema, Model, model } from "mongoose";
import { playerSchema, PlayerShape } from "./player";

interface NecromundaShape extends Document {
    players: [PlayerShape];
}

const necromundaSchema = new Schema({
    players: [playerSchema]
});

const NecromundaModel: Model<NecromundaShape> = model("Necromunda", necromundaSchema);

export { NecromundaShape, necromundaSchema, NecromundaModel };