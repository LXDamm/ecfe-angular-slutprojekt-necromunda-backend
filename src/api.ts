import { v4 as uuidv4 } from 'uuid';
import { CharacterModel, CharacterShape, CharacterStatsShape } from './models/character';
import { GangModel, GangShape } from './models/gang';
import { NecromundaModel, NecromundaShape } from './models/necromunda';
import { PlayerModel, PlayerShape } from './models/player';

const addPlayer = async (req: any, res: any, next: any) => {
    try {
        const generatedPlayerId = uuidv4();
        const playerModel = await PlayerModel.create({ uid: generatedPlayerId, name: req.body.name, gangIds: [] });
        if (playerModel) {
            res.status(200).send({ name: req.body.name, uid: generatedPlayerId });
        }
    } catch (error) {
        res.status(500).send(error);
    }
};

const getPlayers = async (req: any, res: any) => {
    try {
        const players: PlayerShape[] = await PlayerModel.find().exec();
        if (players) {
            res.status(200).send(players);
        } else {
            res.status(404).send('Players not found');
        }
    } catch (error) {
        res.status(500).send(error);
    }
};

const getPlayer = async (req: any, res: any) => {
    try {
        const player = await PlayerModel.findOne({ uid: req.params.playerId }).exec();
        if (player) {
            res.status(200).send(player);
        } else {
            res.status(404).send('Player not found');
        }
    } catch (error) {
        res.status(500).send(error);
    }
};

const addGang = async (req: any, res: any, next: any) => {
    try {
        const playerId: string = req.params.playerId;
        const generatedGangId = uuidv4();
        const createdGang: GangShape = req.body;
        const playerModel = await PlayerModel.updateOne({ uid: playerId }, { $push: { gangIds: generatedGangId } });
        const gangModel = await GangModel.create({ uid: generatedGangId, "name": createdGang.name, "credits": createdGang.credits, "meat": createdGang.meat, "rating": createdGang.rating, "reputation": createdGang.reputation, "wealth": createdGang.wealth, "characterIds": [] });
        if (playerModel && gangModel) {
            res.status(200).send(generatedGangId);
        } else {
            res.status(404).send('Gang could not be created');
        }
    } catch (error) {
        res.status(500).send(error);
    }
};

const getGangs = async (req: any, res: any) => {
    try {
        const player = await PlayerModel.findOne({ uid: req.params.playerId }).exec();
        const gangIds = player?.gangIds;
        const gangs: GangShape[] = await GangModel.aggregate([{ $match: { uid: { $in: gangIds } } }]);
        if (gangs) {
            res.status(200).send(gangs);
        } else {
            res.status(404).send('Gangs not found');
        }
    } catch (error) {
        res.status(500).send(error);
    }
};

const getGang = async (req: any, res: any) => {
    try {
        const gang = await GangModel.findOne({ uid: req.params.gangId }).exec();
        if (gang) {
            res.status(200).send(gang);
        } else {
            res.status(404).send('Gang not found');
        }
    } catch (error) {
        res.status(500).send(error);
    }
};

const updateGang = async (req: any, res: any, next: any) => {
    try {
        const updatedGang: GangShape = req.body;
        const gangModel = await GangModel.findOneAndUpdate({ uid: req.params.gangId }, { $set: { name: updatedGang.name, credits: updatedGang.credits, meat: updatedGang.meat, rating: updatedGang.rating, reputation: updatedGang.reputation, wealth: updatedGang.wealth } });
        if (gangModel) {
            res.status(200).send(gangModel);
        } else {
            res.status(404).send('Gang could not be updated');
        }
    } catch (error) {
        res.status(500).send(error);
    }
};

const addCharacter = async (req: any, res: any, next: any) => {
    try {
        const gangId: string = req.params.gangId;
        const generatedCharacterId = uuidv4();
        const createdCharacter: CharacterShape = req.body;
        const gangModel = await GangModel.updateOne({ uid: gangId }, { $push: { characterIds: generatedCharacterId } });
        const characterModel = await CharacterModel.create({ uid: generatedCharacterId, name: createdCharacter.name, imgUrl: '', stats: createdCharacter.stats });
        if (gangModel && characterModel) {
            res.status(200).send(generatedCharacterId);
        } else {
            res.status(404).send('Character could not be created');
        }
    } catch (error) {
        res.status(500).send(error);
    }
};

const getCharacters = async (req: any, res: any) => {
    try {
        const gang = await GangModel.findOne({ uid: req.params.gangId }).exec();
        console.log(gang);
        const characterIds = gang?.characterIds;
        console.log(characterIds);

        const characters: CharacterShape[] = await CharacterModel.aggregate([{ $match: { uid: { $in: characterIds } } }]);

        console.log(characters);
        if (characters) {
            res.status(200).send(characters);
        } else {
            res.status(404).send('Charcters not found');
        }
    } catch (error) {
        res.status(500).send(error);
    }
};

const getCharacter = async (req: any, res: any) => {
    try {
        const character = await CharacterModel.findOne({ uid: req.params.characterId }).exec();
        if (character) {
            res.status(200).send(character);
        } else {
            res.status(404).send('Character not found');
        }
    } catch (error) {
        res.status(500).send(error);
    }
};

const updateCharacter = async (req: any, res: any, next: any) => {
    try {
        const updatedCharacter: CharacterShape = req.body;
        const characterModel = await CharacterModel.findOneAndUpdate({ uid: req.params.characterId }, {
            $set: {
                name: updatedCharacter.name, stats: updatedCharacter.stats
            }
        });
        if (characterModel) {
            res.status(200).send(characterModel);
        } else {
            res.status(404).send('Character could not be updated');
        }
    } catch (error) {
        res.status(500).send(error);
    }
};

export const api = { addPlayer, getPlayers, getPlayer, addGang, getGangs, getGang, updateGang, addCharacter, getCharacters, getCharacter, updateCharacter }; 