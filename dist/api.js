"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.api = void 0;
var uuid_1 = require("uuid");
var character_1 = require("./models/character");
var gang_1 = require("./models/gang");
var player_1 = require("./models/player");
/*let defaultPlayer = {
    uid: 'fc37610c-5ef9-42ba-854b-46050b67y5ea',
    name: 'Lexx',
    gangIds: []
}

let defaultGang = {
    uid: 'ae2b068a-cdea-4b22-be11-ddd6889e3182',
    name: 'Cyborgs',
    credits: 0,
    meat: 0,
    rating: 0,
    reputation: 0,
    wealth: 0,
    characterIds: []
}

let defaultCharacterStats = {
    m: 4,
    ws: 2,
    bs: 2,
    s: 2,
    t: 2,
    w: 1,
    i: 4,
    a: 2,
    ld: 6,
    cl: 6,
    wil: 6,
    int: 6,
    xp: 2
}

let defaultCharacter = {
    uid: '4b1838d0-7b76-11eb-b97d-9829a64bedbe',
    name: 'LX200',
    stats: defaultCharacterStats
}
*/
var getPlayers = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var players, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, player_1.PlayerModel.find().exec()];
            case 1:
                players = _a.sent();
                if (players) {
                    res.status(200).send(players);
                }
                else {
                    res.status(404).send('Players not found');
                }
                return [3 /*break*/, 3];
            case 2:
                error_1 = _a.sent();
                res.status(500).send(error_1);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
var getPlayerById = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var player, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, player_1.PlayerModel.findOne({ uid: req.params.playerId }).exec()];
            case 1:
                player = _a.sent();
                if (player) {
                    res.status(200).send(player);
                }
                else {
                    res.status(404).send('Player not found');
                }
                return [3 /*break*/, 3];
            case 2:
                error_2 = _a.sent();
                res.status(500).send(error_2);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
/*
const updatePlayerById = async (req: any, res: any, next: any) => {
    try {
        const result = await GangModel.findOneAndUpdate({ uid: req.params.playerId }, { $set: { name: req.body.name } }, {
            returnOriginal: false
        });
        console.log(result);
        if (result) {
            res.status(200).send(result);
        } else {
            res.status(404).send('Gang not found');
        }
    } catch (error) {
        res.status(500).send(error);
    }
};
*/
var updateGangById = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var result, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, gang_1.GangModel.findOneAndUpdate({ uid: req.params.gangId }, { $set: { name: req.body.name } }, {
                        returnOriginal: false
                    })];
            case 1:
                result = _a.sent();
                console.log(result);
                if (result) {
                    res.status(200).send(result);
                }
                else {
                    res.status(404).send('Gang not found');
                }
                return [3 /*break*/, 3];
            case 2:
                error_3 = _a.sent();
                res.status(500).send(error_3);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
var updateCharacterById = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var result, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                console.log(req.body);
                return [4 /*yield*/, character_1.CharacterModel.findOneAndUpdate({ uid: req.params.characterId }, { $set: { name: req.body.name } }).exec()];
            case 1:
                result = _a.sent();
                if (result) {
                    res.status(200).send(result);
                }
                else {
                    res.status(404).send('Character not found');
                }
                return [3 /*break*/, 3];
            case 2:
                error_4 = _a.sent();
                res.status(500).send(error_4);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
var addPlayer = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var generatedPlayerId, playerModel, error_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                generatedPlayerId = uuid_1.v4();
                return [4 /*yield*/, player_1.PlayerModel.create({ uid: generatedPlayerId, name: 'Lexx', gangIds: [] })];
            case 1:
                playerModel = _a.sent();
                if (playerModel) {
                    res.status(200).send({ uid: generatedPlayerId });
                }
                return [3 /*break*/, 3];
            case 2:
                error_5 = _a.sent();
                res.status(500).send(error_5);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
var addGang = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var playerId, generatedGangId, createdGang, playerModel, gangModel, error_6;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                playerId = req.params.playerId;
                generatedGangId = uuid_1.v4();
                createdGang = req.body;
                return [4 /*yield*/, player_1.PlayerModel.updateOne({ uid: playerId }, { $push: { gangIds: generatedGangId } })];
            case 1:
                playerModel = _a.sent();
                return [4 /*yield*/, gang_1.GangModel.create({ "uid": createdGang.uid, "name": createdGang.name, "credits": createdGang.credits, "meat": createdGang.meat, "rating": createdGang.rating, "reputation": createdGang.reputation, "wealth": createdGang.wealth, "characterIds": [] })];
            case 2:
                gangModel = _a.sent();
                if (playerModel && gangModel) {
                    res.status(200).send(generatedGangId);
                }
                else {
                    res.status(404).send('Gang could not be created');
                }
                return [3 /*break*/, 4];
            case 3:
                error_6 = _a.sent();
                res.status(500).send(error_6);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
var addCharacter = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var uid, result, error_7;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                uid = uuid_1.v4();
                return [4 /*yield*/, character_1.CharacterModel.create({ uid: uid })];
            case 1:
                result = _a.sent();
                if (result) {
                    res.status(200).send(uid);
                }
                else {
                    res.status(404).send('Character could not be created');
                }
                return [3 /*break*/, 3];
            case 2:
                error_7 = _a.sent();
                res.status(500).send(error_7);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.api = { getPlayers: getPlayers, addPlayer: addPlayer };
