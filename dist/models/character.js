"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CharacterModel = exports.characterSchema = exports.characterStatsSchema = void 0;
var mongoose_1 = require("mongoose");
var characterStatsSchema = new mongoose_1.Schema({
    m: Number,
    ws: Number,
    bs: Number,
    s: Number,
    t: Number,
    w: Number,
    i: Number,
    a: Number,
    ld: Number,
    cl: Number,
    wil: Number,
    int: Number,
    xp: Number
});
exports.characterStatsSchema = characterStatsSchema;
var characterSchema = new mongoose_1.Schema({
    uid: {
        type: String,
        required: false,
        unique: true
    },
    name: String,
    imgUrl: String,
    stats: characterStatsSchema
});
exports.characterSchema = characterSchema;
var CharacterModel = mongoose_1.model("Character", characterSchema);
exports.CharacterModel = CharacterModel;
