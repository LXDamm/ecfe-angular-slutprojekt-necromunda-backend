"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GangModel = exports.gangSchema = void 0;
var mongoose_1 = require("mongoose");
var gangSchema = new mongoose_1.Schema({
    uid: {
        type: String,
        required: true,
        unique: true
    },
    name: String,
    credits: Number,
    meat: Number,
    rating: Number,
    reputation: Number,
    wealth: Number,
    characterIds: {
        type: [String],
        required: false,
        default: new Array()
    }
});
exports.gangSchema = gangSchema;
var GangModel = mongoose_1.model("Gang", gangSchema);
exports.GangModel = GangModel;
