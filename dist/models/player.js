"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlayerModel = exports.playerSchema = void 0;
var mongoose_1 = require("mongoose");
var playerSchema = new mongoose_1.Schema({
    uid: {
        type: String,
        required: true,
        unique: true
    },
    name: String,
    gangIds: {
        type: [String],
        required: false,
        default: new Array()
    }
});
exports.playerSchema = playerSchema;
var PlayerModel = mongoose_1.model("Player", playerSchema);
exports.PlayerModel = PlayerModel;
